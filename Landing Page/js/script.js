function thankYscroll() {
  if (document.querySelector(".content").classList.contains("scroll")) {
    //alert("scroll is active");
    document.querySelector('.scroll').scrollIntoView({
      block: 'center',
      behavior: 'smooth'
    });
  }
}

$(document).ready(function () {
  manuliupateDom();
});
$(window).resize(function () {
  manuliupateDom();
});
$(document).scroll(function () {
  collapseMenu();
});

function manuliupateDom() {
  var windowW = $(window).width() + 15;
  
  if ($(this).width() < 768) {
    $('.four-4').insertBefore('.three-3');
    $('.four-4').addClass("mx-3 my-5");
    $('.one-1').addClass("px-0");
  } else {
    $('.four-4').insertBefore('.five-5');
    $('.four-4').removeClass("mx-3 mb-4");
    $('.one-1').removeClass("px-0");
    
  }
};

function collapseMenu() {
  var scroll = $(window).scrollTop();
  if(scroll > 50) {
    $('.head-space').addClass('head-space--scroll');
    $('.head').addClass('head--scroll');
  } 
  if(scroll == 0) {
    $('.head-space').removeClass('head-space--scroll');
    $('.head').removeClass('head--scroll');
  }
}
